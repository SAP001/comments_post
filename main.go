package main

import (
	"encoding/json"
	"fmt"
	"gitlab.com/social_net/models"
	"gitlab.com/social_net/repo/mock"
	"net/http"
)

func hello(w http.ResponseWriter, r *http.Request) {
	fmt.Fprint(w, "/hello")
}

func posts(w http.ResponseWriter, r *http.Request) {
	posts := mock.Repo.Posts()
	for i := range posts {
		posts[i].Com = mock.Repo.CommentsByPostID(posts[i].ID)
	}
	b, _ := json.Marshal(posts)
	w.Write(b)
}

func createPost(w http.ResponseWriter, r *http.Request) {
	post := &models.Post{}
	json.NewDecoder(r.Body).Decode(post)

	id := mock.Repo.CreatePost(post)
	b, _ := json.Marshal(id)
	w.Write(b)
}

func createComment(w http.ResponseWriter, r *http.Request) {
	comment := &models.Comment{}
	json.NewDecoder(r.Body).Decode(comment)

	id := mock.Repo.CreateComment(comment)
	b, _ := json.Marshal(id)
	w.Write(b)
}

func main() {
	http.HandleFunc("/hello", hello)

	http.HandleFunc("/posts", posts)
	http.HandleFunc("/createPosts", createPost)
	http.HandleFunc("/createComment", createComment)
	//http.HandleFunc("/hello", hello)

	http.ListenAndServe(":8080", nil)
}
