package models

type Post struct {
	ID      int
	Text    string
	Author  string
	Picture string
	Com     []*Comment
}

type Comment struct {
	ID      int
	Text    string
	Author  string
	Picture string
	PostID  int
}
