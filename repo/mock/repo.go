package mock

import "gitlab.com/social_net/models"

var Repo Repository

type Repository struct {
	lastPostID    int
	lastCommentID int
}

var postsRepo = map[int]*models.Post{
	0: {
		ID:      0,
		Author:  "Артём",
		Picture: "\"https://static.kulturologia.ru/files/u18476/cote.jpg\"",
		Text:    "Смотрите все, какого классного кота я нашёл!",
	},
}

var commnetRepo = map[int]*models.Comment{
	0: {PostID: 0, Author: "Макс", Text: "Действительно классный)"},
}

func (r *Repository) Posts() []*models.Post {
	posts := make([]*models.Post, 0)
	for _, post := range postsRepo {
		posts = append(posts, post)
	}
	return posts
}

func (r *Repository) CreatePost(post *models.Post) int {
	currId := r.lastPostID
	postsRepo[currId] = post
	r.lastPostID++
	return currId
}

func (r *Repository) CreateComment(comment *models.Comment) int {
	currId := r.lastCommentID
	commnetRepo[currId] = comment
	r.lastCommentID++
	return currId
}

func (r *Repository) CommentsByPostID(postID int) []*models.Comment {
	comments := make([]*models.Comment, 0)
	for _, val := range commnetRepo {
		if val.PostID == postID {
			comments = append(comments, val)
		}
	}
	return comments
}
